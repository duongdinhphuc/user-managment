<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
    }
}
class UserSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert([
			'name' => 'Duong Dinh Phuc',
			'gender' => 'm',
			'address' => 'Tu Son - Bac Ninh',
			'phone' => '0364366032',
			'email' => 'phuc05121998@gmail.com',
			'password' => bcrypt('12345'),
			'remember_token' => str_ramdom(10)
		],[
			'name' => 'Nguyen Ngoc Anh',
			'gender' => 'f',
			'address' => 'Tu Son - Bac Ninh',
			'phone' => '0367362183',
			'email' => 'na@gmail.com',
			'password' => bcrypt('12345'),
			'remember_token' => str_ramdom(10)
		]);
	}
}
