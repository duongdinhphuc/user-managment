<?php

namespace App\Admin\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UserController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->id('Id');
        $grid->name('Name');
        $grid->gender('Gender');
        $grid->address('Address');
        $grid->phone('Phone');
        $grid->email('Email');
        $grid->email_verified_at('Email verified at');
        //$grid->password('Password');
        //$grid->remember_token('Remember token');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');
        $grid->paginate(5);
        $grid->perPages([5,15,25,35,45]);
        //disable
        // $grid->disableCreateButton();
        // $grid->disablePagination();
        // $grid->disableFilter();
        // $grid->disableExport();
        // $grid->disableRowSelector();
        // $grid->disableActions();
        //filter
        $grid->filter(function($filter){
            //remove default id filter
            $filter->disableIdFilter();
            //expand
            $filter->expand();
            //filter
            $filter->column(1/2, function ($filter) {
                $filter->like('name','Name');
                $filter->equal('email','Email')->email();
                $filter->in('gender')->checkbox([
                'm'    => 'Male',
                'f'    => 'Female',
                    ]);
                $filter->like('phone','Phone')->mobile();
                });

            $filter->column(1/2, function ($filter) {
            $filter->like('address','Address');
            $filter->between('created_at')->datetime();
            $filter->between('updated_at')->datetime();
                });
            //scope
            $filter->scope('male','Male')->where('gender', 'm');
            $filter->scope('female','Female')->where('gender','f');
            $filter->scope('Has  Address')->where(function ($query) {
                $query->whereNotNull('address');
            });
            $filter->scope('No  Address')->where(function ($query) {
                $query->whereNull('address');
            });
            $filter->scope('Has  Phone')->where(function ($query) {
                $query->whereNotNull('phone');
            });
            $filter->scope('No  Phone')->where(function ($query) {
                $query->whereNull('phone');
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));
        $show->panel()->style('info')->title('Detail');
        $show->id('Id');
        $show->divider();
        $show->name('Name');
        $show->divider();
        $show->address('Address');
        $show->divider();
        $show->phone('phone')->badge();
        $show->divider();
        $show->gender('Gender');
        $show->divider();
        $show->email('Email')->label();
        $show->divider();
        $show->email_verified_at('Email verified at');
        $show->divider();
        $show->password('Password')->badge();
        $show->divider();
        $show->remember_token('Remember token');
        $show->divider();
        $show->created_at('Created at');
        $show->divider();
        $show->updated_at('Updated at');
        //tools
        // $show->panel()->tools(function ($tool){
        //     $tool->disableEdit();
        //     $tool->disableList();
        //     $tool->disableDelete();
        // }); 
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('name', 'Name')->rules('required|min:3',[
            'required' => 'You have not entered information',
            'min' => 'The name must be at least 3 characters'
        ])->help('For example: Duong Dinh Phuc ...');
        $gender = ['m'=>'Male','f'=>'Female'];
        $form->textarea('address','Address')->help('For example: Hoi Quan - Tuong Giang - Tu Son - Bac Ninh ...');
        $form->mobile('phone','Phone')->help('For example: 0364xxxxxx ...');
        $form->select('gender','Gender')->options($gender);
        $form->email('email', 'Email')->rules('required|email',[
            'required' => 'You have not entered information',
            'email' => 'Incorrect email format',
        ])->help('For example: abc@gmail.com');
        $form->datetime('email_verified_at', 'Email verified at')->default(date('Y-m-d H:i:s'))->rules('required',[
            'required' => 'You have not entered information'
        ]);
        $form->password('password', 'Password');
        $form->text('remember_token', 'Remember token');
        //disable tools
        // $form->tools(function($tools){
        //     $tools->disableList();
        //     $tools->disableView();
        //     $tools->disableDelete();
        // });
        // //disable footer
        // $form->footer(function($footer){
        //     $footer->disableReset();
        //     $footer->disableViewCheck();
        //     $footer->disableEditingCheck();
        //     $footer->disableCreatingCheck();
        //     $footer->disableSubmit();
        // }); 
        return $form;
    }
}
